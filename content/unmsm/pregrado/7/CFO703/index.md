---
title: "Mecánica Cuántica II"
lead: "Profundiza en la mecánica cuántica aplicada a sistemas de dos cuerpos, muchos cuerpos, interacción radiación-materia y cuantización del campo electromagnético"
description: "Este curso aborda temas avanzados de la mecánica cuántica, incluyendo el problema de dos cuerpos, teoría de perturbaciones, problema de muchos cuerpos, matriz de densidad, scattering, fotones y átomos, operadores fermiónicos e información cuántica. Está diseñado para estudiantes que hayan completado la primera parte del curso (Mecánica Cuántica I) y cuenten con sólidos conocimientos en física moderna y álgebra lineal"
id: "CFO703"
credits: 5
prerrequisites: [ "Mecánica Cuántica I" ]
semester: 7
contributors: [ "David" ]
featured: false
pdf: false
math: false
draft: false
weight: 703
images: []
toc: true
date: 2023-04-11T16:00:00
lastmod:
  - :git
  - lastmod
  - date
  - publishDate
menu:
  guias:
    parent: "pregrado"
---

{{< infobox-alert text="Se recomienda contar con una base sólida en mecánica cuántica y estar dispuesto a dedicar tiempo y esfuerzo para comprender los conceptos y aplicaciones de los temas avanzados que se abordarán en este curso." />}}

Mecánica Cuántica II es un curso avanzado que profundiza en los conceptos y aplicaciones de la mecánica cuántica estudiados en Mecánica Cuántica I.

El contenido del curso se organiza en los siguientes temas:

1. Simetría rotacional y traslacional en el problema de dos cuerpos
2. Estados ligados en potenciales centrales
3. Teoría de perturbaciones I y II
4. Problema de muchos cuerpos
5. Matriz de densidad
6. Scattering
7. Fotones y átomos
8. Operadores fermiónicos
9. Información cuántica

## Libros recomendados

### Teóricos

{{< img-simple src="TownsendatSouthPole.jpg" alt="Foto de una persona sosteniendo el libro 'A Modern Approach to Quantum Mechanics' de John S. Townsend en el polo sur geográfico." >}}

El libro de John S. Townsend sigue siendo el preferido del profesor Pablo Rivera para el estudio de la segunda mitad del curso de mecánica cuántica.

{{< content_table type="book" >}}
  {{< book_row title="A Modern Approach to Quantum Mechanics" author="John S. Townsend" editorial="University Science Books" year="2000" edition="1ra ed." url="https://drive.google.com/file/d/1upmB4rSmlq92Ry_0iYd67woMTbrWAON7/view?usp=share_link" >}}
  {{< book_row title="Quantum Mechanics: A Modern and Concise Introductory Course" author="Daniel R. Bes" editorial="Springer-Verlag Berlin Heidelberg" year="2012" edition="3ra ed." url="https://drive.google.com/file/d/1jcdwLyPG5nMkNAiESGycMma9fUJmqhqb/view?usp=share_link" >}}
  {{< book_row title="Quantum Mechanics for Scientists and Engineers" author="David A. B. Miller" editorial="Cambridge University Press" year="2008" edition="1ra ed." url="https://drive.google.com/file/d/17cI8CG2j1xIv25COhxheJluPvhpv_X-g/view?usp=share_link" >}}
{{< /content_table >}}

### Teórico-Prácticos

### Prácticos

Los siguientes libros contienen ejercicios o problemas planteados. Contienen muy poco contenido teórico.

## Documentos

## Listas de reproducción

{{< content_table type="playlist" >}}
  {{< playlist_row title="Quantum Mechanics II" channel="" lecturer="James Freericks" videos="46" url="https://www.youtube.com/playlist?list=PLxyBMSdBhYxNWk6Oj1vG7PjE2L6yp6K8x" >}}
  {{< playlist_row title="MIT 8.05 Quantum Physics II, Fall 2013" channel="MIT OpenCourseWare" lecturer="Barton Zwiebach" videos="26" url="https://www.youtube.com/playlist?list=PLUl4u3cNGP60QlYNsy52fctVBOlk-4lYx" >}}
  {{< playlist_row title="MIT 8.06 Quantum Physics III, Spring 2018" channel="MIT OpenCourseWare" lecturer="Barton Zwiebach" videos="100" url="https://www.youtube.com/playlist?list=PLUl4u3cNGP60Zcz8LnCDFI8RPqRhJbb4L" >}}
{{< /content_table >}}

## Sílabos

{{< details "Sílabo del semestre 2023-I" >}}
  {{< pdfjs file="MC2_Sílabo_2023-I_Rivera.pdf" >}}
{{< /details >}}

## Docentes
