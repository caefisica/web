module overleaf

go 1.20

require github.com/PuerkitoBio/goquery v1.8.1

require github.com/go-resty/resty/v2 v2.7.0

require (
	github.com/2captcha/2captcha-go v1.0.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/chromedp/cdproto v0.0.0-20230329100754-6125fc8d7142 // indirect
	github.com/chromedp/chromedp v0.9.1 // indirect
	github.com/chromedp/sysutil v1.0.0 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)
