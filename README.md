# Documentación del CAE-Física 💻 <!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->[![All Contributors](https://img.shields.io/badge/all_contributors-4-orange.svg?style=flat-square)](#contributors-)<!-- ALL-CONTRIBUTORS-BADGE:END -->

[![Netlify](https://api.netlify.com/api/v1/badges/abc167f5-8dee-48db-a7da-3c97c18849be/deploy-status)](https://app.netlify.com/sites/caefis/deploys) [![Momobot CI](https://github.com/caefisica/web/actions/workflows/node.js_ci.yml/badge.svg)](https://github.com/caefisica/web/actions/workflows/node.js_ci.yml) [![Análisis CodeQL](https://github.com/caefisica/web/actions/workflows/analisis_codeql.yml/badge.svg)](https://github.com/caefisica/web/actions/workflows/analisis_codeql.yml) [![Lighthouse CI](https://github.com/caefisica/web/actions/workflows/lighthouse.yml/badge.svg)](https://github.com/caefisica/web/actions/workflows/lighthouse.yml) [![🔎 Enlaces no existentes](https://github.com/caefisica/web/actions/workflows/enlaces_404.yml/badge.svg)](https://github.com/caefisica/web/actions/workflows/enlaces_404.yml)

Bienvenidos al repositorio de la documentación de cada curso del Plan de Estudios de Física 2018 de la Facultad de Ciencias Físicas de la Universidad Nacional Mayor de San Marcos.

- [Nosotros](#nosotros)
  - [Colaboradores](#colaboradores)
- [Deseo colaborar](#contribuciones)
  - [¿Por qué contribuir lo vale?](#¿por-qué-contribuir?)
- [Convenciones](/docs/convenciones.md#convenciones)
  - [Estructura de las carpetas y nombre de los archivos](/docs/convenciones.md#estructura-del-directorio-y-nombre-de-los-archivos)
  - [Formato de texto](/docs/convenciones.md#formato-del-texto)
  - [Colores](/docs/convenciones.md#colores)
  - [Plantillas](/docs/convenciones.md#plantillas)
- [Para desarrolladores](/docs/instalaci%C3%B3n.md#desarrolladores)

## Nosotros

El Centro de Apoyo al Estudiante de Física (CAE-Física) es un grupo de estudiantes universitarios de física enfocados en ayudar a otros estudiantes de física. Nuestro objetivo es proporcionar recursos y apoyo de alta calidad para ayudar a los estudiantes a tener éxito en sus estudios.

### Colaboradores

Este repositorio es mantenido por un equipo de voluntarios dedicados. Si deseas contribuir, por favor consulta la sección "Contribuciones" a continuación.

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tbody>
    <tr>
      <td align="center"><a href="http://totallynotdavid.github.io"><img src="https://avatars.githubusercontent.com/u/20960328?v=4?s=100" width="100px;" alt="David"/><br /><sub><b>David</b></sub></a><br /><a href="#maintenance-totallynotdavid" title="Maintenance">🚧</a> <a href="#security-totallynotdavid" title="Security">🛡️</a> <a href="#research-totallynotdavid" title="Research">🔬</a> <a href="#blog-totallynotdavid" title="Blogposts">📝</a></td>
      <td align="center"><a href="https://github.com/alvaro18101"><img src="https://avatars.githubusercontent.com/u/75409414?v=4?s=100" width="100px;" alt="Alvaro Alejandro Siesquen Abad"/><br /><sub><b>Alvaro Alejandro Siesquen Abad</b></sub></a><br /><a href="#research-alvaro18101" title="Research">🔬</a> <a href="#blog-alvaro18101" title="Blogposts">📝</a></td>
      <td align="center"><a href="https://github.com/Ser-CorD"><img src="https://avatars.githubusercontent.com/u/98802192?v=4?s=100" width="100px;" alt="Ser-CorD"/><br /><sub><b>Ser-CorD</b></sub></a><br /><a href="#research-Ser-CorD" title="Research">🔬</a> <a href="#blog-Ser-CorD" title="Blogposts">📝</a></td>
      <td align="center"><a href="https://github.com/Rifejo"><img src="https://avatars.githubusercontent.com/u/99055529?v=4?s=100" width="100px;" alt="Rifejo"/><br /><sub><b>Rifejo</b></sub></a><br /><a href="#research-Rifejo" title="Research">🔬</a> <a href="#blog-Rifejo" title="Blogposts">📝</a></td>
    </tr>
  </tbody>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

## Contribuciones

¡Agradecemos cualquier contribución de la comunidad! Si tienes experiencia en física o educación y quieres ayudar a otros estudiantes a mejorar en sus estudios, por favor considera contribuir a este repositorio. Algunas formas en las que puedes hacerlo son:

1. **Guías de estudio**: Si has completado un curso y quieres compartir tus conocimientos y recursos con otros estudiantes, puedes crear o actualizar guías de estudio para ese curso. Asegúrate de incluir recomendaciones de libros y otros materiales útiles, y utiliza un lenguaje claro y accesible para facilitar la comprensión de los temas. En general tratamos de:

   - Recomendar libros de estudio.
   - Recomendar canales de Youtube u otra plataforma que aborde temas similares a los sílabos.

2. **Apuntes de Clase**: Si tienes apuntes de clase completos y bien organizados, podrías considerar compartirlos con otros estudiantes. Asegúrate de usar un lenguaje sencillo y claro, y elimina cualquier información innecesaria para mantener los apuntes concisos y fáciles de seguir.

3. **Recomendaciones de libros y otras fuentes de información**: Si conoces libros o canales de Youtube que cubren temas relevantes para los estudiantes de física, compártelos con la comunidad a través de nuestro repositorio. Asegúrate de proporcionar una descripción clara y completa de cada recomendación para que otros puedan evaluar si es relevante para ellos.

### ¿Por qué contribuir?

Si te apasiona la física y quieres ayudar a los nuevos estudiantes a sobresalir en sus asignaturas, contribuir a este proyecto es una gran manera de hacerlo. Al proporcionar tus propias recomendaciones de libros y listas de reproducción de Youtube, puedes ayudar a los nuevos estudiantes a sacar el máximo provecho de sus estudios de física.
